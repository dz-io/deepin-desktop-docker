FROM debian AS builder

# 主参数
ARG \
    # Debian代号
    from=buster \
    # deepin代号
    tag=apricot \
    # 软件仓库
    repo=https://community-packages.deepin.com/deepin/

# 安装debootstrap
RUN apt update && apt install -y debootstrap

RUN \
    # 对debootstrap打补丁
    ln -s /usr/share/debootstrap/scripts/$from /usr/share/debootstrap/scripts/$tag && \
    # 获取根文件系统
    debootstrap --no-check-gpg --arch=$(dpkg --print-architecture) --include=bash $tag /root/rootfs $repo && \
    # 修复sources.list（启用专有源）
    echo "deb [by-hash=force] ${repo} ${tag} main contrib non-free" > /root/rootfs/etc/apt/sources.list && \
    echo "#deb-src ${repo} ${tag} main contrib non-free" >> /root/rootfs/etc/apt/sources.list && \
    # 清理
    rm -rf /root/rootfs/var/cache/* /root/rootfs/tmp/* /root/rootfs/var/log/*

# 从空镜像开始正式构建
FROM scratch

# 署名
MAINTAINER 大泽 <2592455724@qq.com>
LABEL maintainer="大泽 <2592455724@qq.com>"

# 提取rootfs
COPY --from=builder /root/rootfs /

# 设置默认shell
CMD [ "/bin/bash" ]