# deepin-desktop-docker

## 介绍

deepin 桌面版的 docker 镜像

## 安装教程

Docker 传统，无需安装  
如果你真想安装的话

```bash
docker pull daze456/deepin-desktop
```

从 gitee（码云）构建并安装

```bash
docker build --pull --rm -t daze456/deepin-desktop:latest https://gitee.com/daze456/deepin-desktop-docker/raw/master/Dockerfile
```

## 使用说明

和 Debian 镜像用法一样：

```bash
docker run -it daze456/deepin-desktop:latest
```

## 高级用户

其实这个 Dockerfile 理论上也可以用于构建其他基于 Debian 的发行版，Dockerfile 里有注释  
PS: 这个 Dockerfile 的优点是构建全程对主机完全隔离，不用担心把权限搞乱

## 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

## 许可协议

```
   Copyright (c) 2020 大泽
   This software is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2.
   You may obtain a copy of Mulan PSL v2 at:
            http://license.coscl.org.cn/MulanPSL2
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
   See the Mulan PSL v2 for more details.
```
